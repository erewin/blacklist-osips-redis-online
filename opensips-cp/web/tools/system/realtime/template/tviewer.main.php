<?php
 /*
 * Copyright (C) 2011 OpenSIPS Project
 *
 * This file is part of opensips-cp, a free Web Control Panel Application for 
 * OpenSIPS SIP server.
 *
 * opensips-cp is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * opensips-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


// load table content

if(!isset($_GET['redisdbid'])) {

    $sql = $link->query('select ipaddress,id from realtime');

    if(!$sql) echo $link->errno." ".$link->errinfo;

    while($ip = $sql->fetch(PDO::FETCH_ASSOC)) {
        //var_dump($ip);
        $ipaddress[] = $ip;
    }

    //choosing IP
    //var_dump($ipaddress);
    echo "<form name=dateinput method=\"GET\" action=".$_SERVER['PHP_SELF'].">";
    //echo "<input type=hidden name=\"submenu_item_id\" value=\"0\"/>";
    echo "<select id=\"action\" name=\"redisdbid\">";


    foreach ($ipaddress as $value) {
        echo "<option value=".$value['id'].">".$value['ipaddress']."</option>";
    }
    echo "</select>";
    echo "<input type=submit value='Submit' />";
    echo "</form>";

}

$dbid = $_GET['redisdbid'];
if(!$dbid) {
    echo "Choose IP for stats";
    exit;
}


//Start show main screen

echo  "<p align=left>
<ul><li align=left>Red rows means one of condition (ACD or ASR) are False</li>
<li align=left>Use Search field to find number start from value. 
If you wnat to search middle part of number, use * as prefix. 
For example value '*500' will find all numbers include 500, like +1728500123. </li>
<li align=left>Use REFRESH button to update information in table with same filters</li>
</ul><p>";

$numfilter = '';
if($_GET['filter']) $numfilter = $_GET['filter'];

//get data from user checked
if($_GET['typeA']) $filter_type['A'] = $_GET['typeA'];
if($_GET['typeB']) $filter_type['B'] = $_GET['typeB'];
if($_GET['typeAB']) $filter_type['AB'] = $_GET['typeAB'];
if($_GET['status_alerted']) $filter_status['red'] = $_GET['status_alerted'];
if($_GET['status_normal']) $filter_status['green'] = $_GET['status_normal'];

//default filter settings
if(count($filter_type)==0)
    $filter_type = array('A' => 'checked',
                        'B' => 'checked',
                        'AB' => 'checked'
    );
if(count($filter_status)==0)
    $filter_status = array('green' => 'checked');

echo "<form name=refresh method=\"GET\" action=".$_SERVER['PHP_SELF'].">";
echo "<table width='100%'>";

//show filter text and button
echo "<tr><td>";
    //echo "<form name=refresh method=\"GET\" action=".$_SERVER['PHP_SELF'].">";
    echo "<input type=hidden id=action name='redisdbid' value=".$dbid." />";

    echo "<fieldset style='height: auto'><legend>Number types to show</legend>";
    echo "<input type='checkbox' id='typeA' name='typeA' value='checked' ".$filter_type['A']." />";
    echo "<label for='typeA'>Number A</label><br>";

    echo "<input type='checkbox' id='typeB' name='typeB' value='checked' ".$filter_type['B']." />";
    echo "<label for='typeA'>Number B</label><br>";

    echo "<input type='checkbox' id='typeAB' name='typeAB' value='checked'".$filter_type['AB']."/>";
    echo "<label for='typeA'>Number AB</label><br>";
    echo "</fieldset>";
echo "</td><td>";

    echo "<fieldset style='height: auto'><legend>Status</legend>";
    echo "<input type='checkbox' id='status_alerted' name='status_alerted' value='checked'".$filter_status['red']."/>";
    echo "<label for='typeA'>Show Alerted</label><br>";
    echo "<input type='checkbox' id='status_normal' name='status_normal' value='checked'".$filter_status['green']."/>";
    echo "<label for='typeA'>Show Normal</label><br>";
    echo "</fieldset>";
echo "</td></tr>";

echo "<tr><td colspan=2 align=center>";
    echo "<br><label for='filter'>Search number: </label> ";
    echo "<input type=text id=action name='filter' value='$numfilter' /> ";
    //echo "<input class='searchButton' type=submit value='Apply filter' />";
    //echo "</form>";
echo "</td></tr>";
//show refresh button

echo "<tr><td align=center>";
    //echo "<form name=refresh method=\"GET\" action=".$_SERVER['PHP_SELF'].">";
    echo "<input type=hidden id=action name='redisdbid' value=".$dbid." />";
    echo "<input class='searchButton' type=submit value='Refresh' />";
    echo "<input form=back class='searchButton' type=submit value='Back' />";
    //echo "</form>";

echo "</td><td>";

//show back to choose IP
    //echo "<form id=back name=back method=\"GET\" action=".$_SERVER['PHP_SELF'].">";
    echo "<div style='display: inline-block'>";
    echo "<input class='searchButton' type=submit value='Apply filter' />";
    echo "</div>";
  //  echo "</form>";

echo "</td></tr></table>";
echo "</form>";

echo "<form id=back name=back method=\"GET\" action=".$_SERVER['PHP_SELF'].">";
echo "</form>";

//var_dump(array_keys($filter_type));
//show only A 
//show only B
//show only AB
//show only alerted
//show only normal

$sql =  $link->query("SELECT ACDSet,ASRSet,active from realtime where id = $dbid limit 1");
$result = $sql->fetch(PDO::FETCH_ASSOC);


$acd_limit = $result['ACDSet'];
$asr_limit = $result['ASRSet'];
$is_ip_active = $result['active'];

/* echo $acd_limit;
echo $asr_limit;
 */
require 'Predis/Autoloader.php';
Predis\Autoloader::register();

$client = new Predis\Client('tcp://127.0.0.1:6379');

$client->select($dbid); 

$headers    = array('Filter','Answered','Missed','Duration','count','ACD','ACD Target','ASR %','ASR Target','Number');
$redis_keys = array('number','answered','missed','duration','count','ACD','ACDset'    ,'ASR'  ,'ASRset'    ,'type');

$my_table = new Table;
$my_table->start('class="ttable" width="95%" cellspacing="2" cellpadding="2" border="0"');
$my_table->th_array($headers);

$value  = $client->keys($numfilter.'*');

try {

    foreach ($value as $filter) {
        $key = $client->hgetall($filter);
        
        //filter by type
        if (!in_array($key['type'],array_keys($filter_type))) continue;

        foreach(array('answered','missed','duration') as $param) 
            if($key[$param] == NULL) $key[$param] = 0;
        
   
        $key['count']   = Stat::calc_count($key);
        $key['ACD']     = Stat::calc_acd($key);
        $key['ACDset']  = $acd_limit;
        $key['ASR']     = Stat::calc_asr($key);
        $key['ASRset']  = $asr_limit;
        $key['number']  = $filter;

        switch (Stat::getstatus($key,$acd_limit,$asr_limit)) {
            case 'EnabledNotActive':
                $status = 'green';
                break;
            case 'EnabledAndActive':
                $status = 'red';
                break;
            default:
                $status = 'black';
                break;
        }

        if ($is_ip_active == '0') $status = 'black';
        if (!in_array($status,array_keys($filter_status))) continue;

        
        $my_table->tr_start("style=\"color: $status\"");
        foreach($redis_keys as $param)
            $my_table->td($key[$param],'align=center');

        $my_table->tr_end();
    }

} catch(Exception $err) {
    echo "error $err";
}


$my_table->end();

echo $my_table->columns_count." - ".$my_table->row_count;

class Table {
    public $columns_count = 0;
    public $row_count = 0;

    function start($params = '') {
        //$this->columns_count = $columns_count;
        echo "<table $params>";
        //return $this->columns_count;
    }

    public function th_array($array = [], $params = '') {
       
        foreach($array as $value) {
            $str .= "<th $params>$value</th>";
            $i++;
        }
        echo $str;
        $this->columns_count = $i;
    }    

    public function tr_start( $params = '') {
        echo "<tr $params>";
        $this->row_count += 1;
    }
    public function tr_end() {
        echo "</tr>";
    }
    public function td($string, $params = '') {
        echo "<td $params>$string</td>";
    }
    public function td_array($array = [], $params = '') {
        
        for ($i=0; $i < count($array); $i++) {
            if ($array[$i] == '') $array[$i] = 'empty!';
            
            $str .= "<td $params>".$array[$i]."</td>";
            
            
        }
        echo $str;

    }

    public function end() {
        echo "</table>";
    }

}
class Stat {
    public function calc_count($array = []) {
    
        $array['count'] = $array['answered']+$array['missed'];
    
        return $array['count'];
    }
    public function calc_acd($array = []) {
        if ($array['duration']==0) return "0";
        $array['ACD'] = $array['duration']/$array['answered'];
        $result = sprintf("%d", $array['ACD']);
        return $result;
    }
    public function calc_asr($array = []) {
        if ($array['missed']==0) return "100";
        $array['ASR'] = $array['answered']/($array['answered']+$array['missed'])*100;
        $result = sprintf("%d", $array['ASR']);
        return $result;
    }
    public function getstatus($array = [],$acd_limit_var = 0,$asr_limit_var = 0) {
        

        if ($array['ACD'] < $acd_limit_var) {
            //echo $array['ACD'] ." < ". $acd_limit_var."<br>";
            return 'EnabledAndActive';
        }

        if ($array['ASR'] < $asr_limit_var) {
            //echo $array['ASR']." < ".$asr_limit_var."<br>";
            return 'EnabledAndActive';
        }
        
        return 'EnabledNotActive';
    }
    
}

?>